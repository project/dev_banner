<?php

/**
 * @file
 * dev_banner admin pages
 */


/**
 * Displays the form for the standard settings tab.
 *
 * @return
 *   array A structured array for use with Forms API.
 */
function dev_banner_admin_settings() {
  $banner_defs = _dev_banner_definitions();
  
  
  // path to custom images
  $custom_image_path = file_directory_path() . '/dev_banner';
  

  $form['dev_banner_general'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('General settings'),
    '#collapsible'   => TRUE,
    '#collapsed'     => FALSE,
  );
  $form['dev_banner_general']['dev_banner_enabled'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('enable Development Banner'),
    '#default_value' => variable_get('dev_banner_enabled', TRUE),
    '#description'   => t('Click to display Development Banner'),
  );

  $form['dev_banner_general']['dev_banner_position'] = array(
    '#type'          => 'radios',
    '#title'         => t('Development Banner position'),
    '#options' => array(t('left'), t('right')),
    '#default_value' => variable_get('dev_banner_position', 0),
    '#prefix'        => '<div class="dpo-start">',
    '#suffix'        => '</div>',
  );

  $form['dev_banner_images'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('Select images'),
    '#collapsible'   => TRUE,
    '#collapsed'     => FALSE,
  );
  $form['dev_banner_images']['advice'] = array(
    '#type'          => 'markup',
    '#value'         => t('<p>Select a set of images for displaying Development Banners.  You may create your own custom images. They must be in PNG format, measure 72px by 72px, and located in the dev_banner subdirectory of your files path ('. $custom_image_path .').</p>'),
  );

  // build options
  $options = array();
  foreach ($banner_defs['styles'] as $style) {
    $options[] = preg_replace('@_@', ' ', t(ucfirst($style)));
  }
  
  $default_image = variable_get('dev_banner_image_set', 0);
  $form['dev_banner_images']['dev_banner_image_set'] = array(
    '#type'          => 'radios',
    '#options'       => $options,
    '#default_value' => $default_image,
    '#prefix'        => '<div class="si-start">',
    '#suffix'        => '</div>',
  );

  
  $form['mapping'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('URL mapping'),
    '#collapsible'   => TRUE,
    '#collapsed'     => FALSE,
  );
  $form['mapping']['advice'] = array(
    '#type'          => 'markup',
    '#value'         => t('<p>Enter host names for your development, staging, and test servers. The name should contain only the fully qualified domain name; i.e. <em>www.myserver.com</em>. If the host name matches the current server, the corresponding banner will be displayed.</p><p>If the site includes a port number, include it; for "http://staging.anothersite.com:8080", you would enter "staging.anothersite.com:8080"</p><p>If the site\'s base URL is in a subdirectory, include it (but not the following slash); for "http://www.testsite.com/stage", you would enter "www.testsite.com/stage"</p>'),
  );
  $form['mapping']['dev_banner_url_devel'] = array(
    '#type'          => 'textfield',
    '#title'         => t('development server host name'),
    '#size'          => 60,
    '#maxlength'     => 128,
    '#default_value' => variable_get('dev_banner_url_devel', ''),
    '#collapsible'   => TRUE,
    '#collapsed'     => FALSE,
  );
  $form['mapping']['dev_banner_url_stage'] = array(
    '#type'          => 'textfield',
    '#title'         => t('staging server host name'),
    '#size'          => 60,
    '#maxlength'     => 128,
    '#default_value' => variable_get('dev_banner_url_stage', ''),
    '#collapsible'   => TRUE,
    '#collapsed'     => FALSE,
  );
  $form['mapping']['dev_banner_url_test'] = array(
    '#type'          => 'textfield',
    '#title'         => t('test server host name'),
    '#size'          => 60,
    '#maxlength'     => 128,
    '#default_value' => variable_get('dev_banner_url_test', ''),
    '#collapsible'   => TRUE,
    '#collapsed'     => FALSE,
  );

  return system_settings_form($form);
}


